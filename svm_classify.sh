#!/usr/bin/env bash

# Stefan Behr
# LING 572
# Homework 7
# SVM Classify script

# $1 is path to test data file (in libSVM data format)
# $2 is path to model file (in libSVM model format)
# $3 is path to system output file

python2.7 classify.py $1 $2 $3