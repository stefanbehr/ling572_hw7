#!/usr/bin/env bash

# Stefan Behr
# LING 572
# Homework 7
# Q2 script

if [ ! -d q2 ]
then
    echo Creating q2...
    mkdir q2
fi

# file paths
EX="/dropbox/12-13/572/hw7/examples"
TEST="$EX/test"

# 1
echo Testing with model.1
./svm_classify.sh $TEST q1/model.1 q2/sys.1

# 2
echo Testing with model.2
./svm_classify.sh $TEST q1/model.2 q2/sys.2

# 3
echo Testing with model.3
./svm_classify.sh $TEST q1/model.3 q2/sys.3

# 4
echo Testing with model.4
./svm_classify.sh $TEST q1/model.4 q2/sys.4

# 5
echo Testing with model.5
./svm_classify.sh $TEST q1/model.5 q2/sys.5
