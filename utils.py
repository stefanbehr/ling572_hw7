# Stefan Behr
# LING 572
# Homework 7

"""Utilities module for LING 572, Homework 7"""

import math

def pprint_matrix(matrix):
	"""
	Given a matrix in the form of a dict of 
	dicts, return pretty-print string version 
	of matrix.
	"""
	row_labels = [str(x) for x in sorted(matrix.keys())]
	col_labels = [str(x) for x in sorted(matrix[matrix.keys()[0]].keys())]
	rows = ["\t".join([""] + col_labels)]
	for row_label in row_labels:
		row = matrix[row_label]
		row = [str(row[col_label]) for col_label in col_labels]
		row.insert(0, row_label)
		rows.append("\t".join(row))
	return "\n".join(rows)

def parse_line(line):
	"""
	Given a line from a file, returns a list of 
	all but the first whitespace-separated strings in 
	the line. Trailing whitespace is ignored.
	"""
	columns = line.strip().split()
	return [columns[0], " ".join(columns[1:])]

def split_fv_pair(pair):
	"""Given a string value in the form 
	<int1>:<int2>, return a tuple of the form 
	(<int1>, <int2>).
	"""
	return tuple(map(int, pair.split(":")))

def process_support_vector(line):
	"""
	Given a string representing a support vector 
	associated with its weight, return a tuple containing 
	the weight, followed by a dict of feature-value pairs.
	"""
	line = line.split()
	weight = float(line.pop(0))
	vector = {feat: value for feat, value in map(split_fv_pair, line)}
	return (weight, vector)

def inner_product(v, w):
	"""
	Returns the Euclidean inner product 
	between two vectors v and w, each given 
	in the form of a dictionary of feature-value 
	pairs. Absence of a particular feature implies 
	that its value in the vector is 0.
	"""
	shared_feats = set(v.keys()) & set(w.keys())
	return sum(v[feat] * w[feat] for feat in shared_feats)

def vector_difference(v, w):
	"""
	Returns the vector resulting from subtracting the 
	vector v from the vector w. Vectors are given and 
	returned in the form of dictionaries of feature-value 
	pairs. Absent features have an assumed value of 0.
	"""
	feat_space = set(v.keys()) | set(w.keys())
	return {feat: v.get(feat, 0) - w.get(feat, 0) for feat in feat_space}

def vector_norm(v):
	"""
	Given a vector, returns its norm value.
	"""
	return math.sqrt(inner_product(v, v))

class Model:
	"""
	Model class for storing SVM models
	"""
	fields = [
				"kernel_type",
				"degree",
				"gamma",
				"coef0",
				"total_sv",
				"rho",
				"label",
			]

	def __init__(self, model_path):
		with open(model_path) as model_file:
			model_header, support_vectors = model_file.read().split("SV")
			
			model_header = model_header.strip().split("\n")
			model_header = {field: value for field, value in map(parse_line, model_header)}

			# set object attributes to values if present, else None
			for field in Model.fields:
				setattr(self, field, model_header.get(field, None))

			# cast some fields to floats
			for field in ["degree", "gamma", "coef0", "rho"]:
				value = getattr(self, field)
				if value is not None:
					setattr(self, field, float(value))

			# cast some fields to ints
			for field in ["total_sv"]:
				setattr(self, field, int(getattr(self, field)))

			self.label = self.label.split(" ")

			# store support vectors
			support_vectors = support_vectors.strip().split("\n")
			self.vectors = map(process_support_vector, support_vectors)

			# define and store kernel function
			if self.kernel_type == "linear":
				def kernel_function(v, w):
					return inner_product(v, w)
			elif self.kernel_type == "polynomial":
				def kernel_function(v, w):
					return (self.gamma * inner_product(v, w) + self.coef0) ** self.degree
			elif self.kernel_type == "rbf":
				def kernel_function(v, w):
					return math.exp(-self.gamma * vector_norm(vector_difference(v, w)) ** 2)
			elif self.kernel_type == "sigmoid":
				def kernel_function(v, w):
					return math.tanh(self.gamma * inner_product(v, w) + self.coef0)
			self.kernel_function = kernel_function

	def decode(self, instance):
		"""
		Given an instance (in the form of a dictionary of 
		feature-value pairs), returns a class label for that 
		instance according to classification by the model. Returns 
		class label packaged into a tuple along with f(x) calculated 
		on instance.
		"""
		fx = sum(map(lambda sv: sv[0] * self.kernel_function(sv[1], instance), self.vectors)) - self.rho
		if fx > 0:
			label = self.label[0]
		else:
			label = self.label[1]
		return (fx, label)