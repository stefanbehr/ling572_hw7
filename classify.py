#!/usr/bin/env python

# Stefan Behr
# LING 572
# Homework 7

"""SVM decoder"""

import argparse
import utils

if __name__ == "__main__":
	# Parse CL arguments
	parser = argparse.ArgumentParser(description="An SVM decoder script")
	parser.add_argument("test_path", action="store", help="Path to test data for classification.")
	parser.add_argument("model_path", action="store", help="Path to model used for decoding.")
	parser.add_argument("output_path", action="store", help="System output.")
	args = parser.parse_args()

	# store model
	model = utils.Model(args.model_path)
	# initialize confusion matrix
	confusion = {rlabel: {clabel: 0 for clabel in model.label} for rlabel in model.label}

	# iterate through test instances
	with open(args.test_path) as test_file:
		with open(args.output_path, "w") as output_file:
			correct = 0
			predictions = 0
			for line in test_file:
				line = line.strip().split()
				# non-empty line
				if line:
					gold_label = line.pop(0)
					instance = {feat: value for feat, value in map(utils.split_fv_pair, line)}
					fx, sys_label = model.decode(instance)

					confusion[gold_label][sys_label] += 1
					predictions += 1
					if gold_label == sys_label:
						correct += 1

					print >> output_file, "{0} {1} {2}".format(gold_label, sys_label, fx)

	print "Confusion matrix (rows=truth, cols=prediction):"
	print utils.pprint_matrix(confusion)
	print "Test accuracy: {0}".format(float(correct) / predictions)