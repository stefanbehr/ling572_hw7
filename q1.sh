#!/usr/bin/env bash

# Stefan Behr
# LING 572
# Homework 7
# Q1 script

# libSVM executables
SVM="/NLP_TOOLS/ml_tools/svm/libsvm/latest"
SVMTRAIN="$SVM/svm-train"
SVMPREDICT="$SVM/svm-predict"

# training, test data
EX="/dropbox/12-13/572/hw7/examples"
TEST="$EX/test"
TRAIN="$EX/train"

# set up dir structure
if [ ! -d q1 ]
then
    echo Creating q1...
    mkdir q1
fi

# expt id 1
echo Training 1
$SVMTRAIN -t 0 $TRAIN q1/model.1
echo Getting training acc 1
$SVMPREDICT $TRAIN q1/model.1 q1/train.out.1
echo Testing 1
$SVMPREDICT $TEST q1/model.1 q1/test.out.1

# expt id 2
echo Training 2
$SVMTRAIN -t 1 -g 1 -r 0 -d 2 $TRAIN q1/model.2
echo Getting training acc 2
$SVMPREDICT $TRAIN q1/model.2 q1/train.out.2
echo Testing 2
$SVMPREDICT $TEST q1/model.2 q1/test.out.2

# expt id 3
echo Training 3
$SVMTRAIN -t 1 -g 0.1 -r 0.5 -d 2 $TRAIN q1/model.3
echo Getting training acc 3
$SVMPREDICT $TRAIN q1/model.3 q1/train.out.3
echo Testing 3
$SVMPREDICT $TEST q1/model.3 q1/test.out.3

# expt id 4
echo Training 4
$SVMTRAIN -t 2 -g 0.5 $TRAIN q1/model.4
echo Getting training acc 4
$SVMPREDICT $TRAIN q1/model.4 q1/train.out.4
echo Testing 4
$SVMPREDICT $TEST q1/model.4 q1/test.out.4

# expt id 5
echo Training 5
$SVMTRAIN -t 3 -g 0.5 -r -0.2 $TRAIN q1/model.5
echo Getting training acc 5
$SVMPREDICT $TRAIN q1/model.5 q1/train.out.5
echo Testing 5
$SVMPREDICT $TEST q1/model.5 q1/test.out.5